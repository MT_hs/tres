/*
  GRUPO 03
  Arthur Mittmann Krause e Matheus Toazza Tura
*/
%{
#include "cc_tree.h"
#include "parser.h" //arquivo automaticamente gerado pelo bison
#include "cc_dict.h"
#include "cc_misc.h"
#include "main.h"

struct dict_element* table_add();
char* retira_aspas();

extern int start;
int myLineNumber = 1;
int tokenType;
int exists=0;
//extern comp_dict_t* dict;
extern comp_stack_t *stack;
comp_dict_item_t* item;
comp_dict_item_t* aux_item;
dict_element* element;
dict_element* aux_element;

%}

DIGIT    [0-9]
STRING (\\.|[^"])*
ASCII (\\.|[^"])
ID [_a-zA-Z][_a-zA-Z0-9]*
%x COMMENT
%x ASPY

%%

int  return TK_PR_INT; 
float  return TK_PR_FLOAT; 
bool  return TK_PR_BOOL; 
char  return TK_PR_CHAR; 
string return TK_PR_STRING; 
if  return TK_PR_IF; 
then  return TK_PR_THEN;
else  return TK_PR_ELSE;
while return TK_PR_WHILE;
do return TK_PR_DO;
input  return TK_PR_INPUT;
output  return TK_PR_OUTPUT;
return  return TK_PR_RETURN;
const  return TK_PR_CONST;
static  return TK_PR_STATIC;
foreach  return TK_PR_FOREACH;
for  return TK_PR_FOR;
switch  return TK_PR_SWITCH;
case  return TK_PR_CASE;
break  return TK_PR_BREAK;
continue  return TK_PR_CONTINUE;
class  return TK_PR_CLASS;
private  return TK_PR_PRIVATE;
public  return TK_PR_PUBLIC;
protected  return TK_PR_PROTECTED;
"<="  return TK_OC_LE;
">="  return TK_OC_GE;
"=="  return TK_OC_EQ;
"!="  return TK_OC_NE;
"&&"  return TK_OC_AND;
"||"  return TK_OC_OR;
"<<"  return TK_OC_SL;
">>"  return TK_OC_SR;
\ 
\t 
[\,\;\:\(\)\[\]\{\}\+\-\*\/\<\>\=\!\&\$\%\#\^] return (int)*(yytext);

false  tokenType = SIMBOLO_LITERAL_BOOL; yylval.valor_simbolico_lexico = table_add(); return TK_LIT_FALSE;
true  tokenType = SIMBOLO_LITERAL_BOOL; yylval.valor_simbolico_lexico = table_add(); return TK_LIT_TRUE;
{DIGIT}+  tokenType = SIMBOLO_LITERAL_INT; yylval.valor_simbolico_lexico = table_add(); return TK_LIT_INT;
{DIGIT}+"."{DIGIT}+ tokenType = SIMBOLO_LITERAL_FLOAT; yylval.valor_simbolico_lexico = table_add(); return TK_LIT_FLOAT;
{ID} tokenType = SIMBOLO_IDENTIFICADOR; yylval.valor_simbolico_lexico = table_add(); return TK_IDENTIFICADOR;
'{ASCII}' tokenType = SIMBOLO_LITERAL_CHAR; yylval.valor_simbolico_lexico = table_add(); return TK_LIT_CHAR;
\"{STRING}+\" tokenType = SIMBOLO_LITERAL_STRING; yylval.valor_simbolico_lexico = table_add(); return TK_LIT_STRING;

"/*" {BEGIN(COMMENT);}
<COMMENT>"\*/" {BEGIN(INITIAL);}
<COMMENT>. {}
<COMMENT>"\n" {myLineNumber++;} 

"//".*\n myLineNumber++;
\n myLineNumber++;
.|\n return TOKEN_ERRO;

%%
struct dict_element* table_add()
{
	element = malloc(sizeof(dict_element));
	element->token = tokenType;
	element->line = myLineNumber;
	element->declared = false;
	char* key = (char*)calloc(yyleng+2, sizeof(char));
	key[0] = element->token + 48;
	switch(tokenType)
	{
		case SIMBOLO_LITERAL_STRING: 
			element->literal.s = retira_aspas();
			strcpy(key+1,element->literal.s); 
			break;
		case SIMBOLO_LITERAL_INT: 
			element->literal.i = atoi(yytext);
			strcpy(key+1,yytext);
			break;
		case SIMBOLO_LITERAL_FLOAT: 
			element->literal.f = atof(yytext); 
			strcpy(key+1,yytext);
			break;
		case SIMBOLO_LITERAL_CHAR:
			element->literal.c = yytext[1]; 
			key[1] = element->literal.c;
			break;
		case SIMBOLO_LITERAL_BOOL: 
			if(yytext == "true") 
				element->literal.b = true; 
			else 
				element->literal.b = false;
			break;
		case SIMBOLO_IDENTIFICADOR: 
			element->literal.s = (char*)calloc(yyleng+1, sizeof(char)); 
			strcpy((char*)element->literal.s, yytext);
			strcpy(key+1,yytext); 
			break;
		default: break;
	}

/*int i = 0;
exists = 0;
    for (i = 0; i < stack->dict->size ; i++) 	
    {
	item = stack->dict->data[i];
	if(item != NULL)
            if (strcmp(item->key,key)==0) 
	    {
		exists=1;
		aux_element = stack->dict->data[i]->value;
	    	if(aux_element->token == SIMBOLO_LITERAL_STRING || aux_element->token == SIMBOLO_IDENTIFICADOR)
	            free(aux_element->literal.s);
	    	free(aux_element);
	    	dict_remove(stack->dict, key);
	    	dict_put(stack->dict, key , element);
		break;
	    }

    }
    if(! exists)*/

    element->declared = false;
    element->vector = false;
    element->function = false;
    element->variable = false;
    element->type = 0;
    element->coertion = 0;
    element->size = 0;
    element->type_size = 0;
    element->param_counter = 0;
	element->dim = 0;
    element->INTlist = NULL;
    element->INTlist_tail = NULL;

	int i = 0;
	for(i=0; i< 20; i++)
	    element->dimsize[i] = 0;

	dict_put(stack->dict, key , element);
	dict_get(stack->dict, key);

    free(key);
return (struct dict_element*)element;
}




char* retira_aspas()
{
	char* string = calloc(yyleng-1, sizeof(char));
	memcpy(string, yytext+1, yyleng-2);
	return string;
}

