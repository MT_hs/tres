/*
	GRUPO 03
	Arthur Mittmann Krause e Matheus Toazza Tura 
*/
%{
#include <stdio.h>
#include <stdlib.h>
#include "cc_tree.h"
#include "cc_misc.h"
#include "cc_ast.h"
#include "cc_gv.h"
extern comp_tree_t* tree;
comp_stack_t *stack;
int function_actual_type = 0;
int index_count = 0;
extern int dim[20];
int param_count = 0;
bool param_period = false;
Ilist* param_cursor = NULL;
dict_element* function_actual_pointer = NULL;
extern int escopo_global;
//extern dict_element* dict;
%}



%union {
int element_type;
comp_tree_t *ast;
struct dict_element *valor_simbolico_lexico;
}

%type<ast> programa
%type<ast> CORPO
%type<ast> DECL_FUNC
%type<ast> EXPRESSAO
%type<ast> EXPRESSAO_ARIT
%type<ast> EXPRESSAO_LOG
%type<ast> TERMO
%type<ast> TERMO_AT
%type<ast> FATOR
%type<ast> CHAMADA_FUNC
%type<ast> BLOCO_DE_COMANDOS
%type<ast> LISTA_DE_COMANDOS
%type<ast> LISTA_DE_EXPRESSOES
%type<ast> DECLARACAO
%type<ast> ATRIBUICAO
%type<ast> ENTRADA_SAIDA
%type<ast> SHIFT
%type<ast> RBC
%type<ast> COMANDO
%type<ast> IF
%type<ast> WHILE
%type<ast> RETURN
%type<ast> EH_VETOR
%type<ast> ARGUMENTOS
%type<ast> LISTA_DE_ARGUMENTOS
%type<ast> LITERAL
%type<ast> LISTA_DE_INDICES
%type<ast> LISTA_DE_DIMENSOES
%type<valor_simbolico_lexico> TK_IDENTIFICADOR
%type<valor_simbolico_lexico> TK_LIT_INT
%type<valor_simbolico_lexico> TK_LIT_FLOAT
%type<valor_simbolico_lexico> TK_LIT_STRING
%type<valor_simbolico_lexico> TK_LIT_CHAR
%type<valor_simbolico_lexico> TK_LIT_TRUE
%type<valor_simbolico_lexico> TK_LIT_FALSE
%type<element_type> TIPO_USUARIO
%type<element_type> TIPO_PRIMITIVO
%type<element_type> TIPO_USUARIO_CONST
%type<element_type> TIPO_USUARIO_STATIC
%type<element_type> TIPO_USUARIO_CONST_STATIC

/* Declaração dos tokens da linguagem */

%token TK_IDENTIFICADOR
%token TK_LIT_INT
%token TK_LIT_FLOAT
%token TK_LIT_FALSE
%token TK_LIT_TRUE
%token TK_LIT_CHAR
%token TK_LIT_STRING
%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token TK_PR_IF
%right TK_PR_THEN
%right TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_PR_CONST
%token TK_PR_STATIC
%token TK_PR_FOREACH
%token TK_PR_FOR
%token TK_PR_SWITCH
%token TK_PR_CASE
%token TK_PR_BREAK
%token TK_PR_CONTINUE
%token TK_PR_CLASS
%token TK_PR_PRIVATE
%token TK_PR_PUBLIC
%token TK_PR_PROTECTED
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token TK_OC_SL
%token TK_OC_SR
%token TOKEN_ERRO

%right '('
%left ')'
%right '['
%left ']'
%left '+' '-'
%left '*' '/' 
%left  TK_OC_LE TK_OC_GE TK_OC_EQ TK_OC_NE TK_OC_AND TK_OC_OR TK_OC_SL TK_OC_SR
%left '<' '>'
%right ','


%%
/* Regras (e ações) da gramática */

programa: CORPO {
gv_declare_checknull(AST_PROGRAMA, tree, NULL);
tree->value = createAstElement(AST_PROGRAMA,NULL);
tree_insert_checknull(tree, $1);
};

CORPO: DECL_NOVO_TIPO CORPO{$$ = $2;}
| DECL_VAR_GLOBAL CORPO{$$ = $2;}
| DECL_FUNC CORPO {$$ = $1; tree_insert_checknull($$, $2);}
|%empty{$$=NULL;}
;



DECL_NOVO_TIPO: TK_PR_CLASS TK_IDENTIFICADOR '[' ENCAPSULAMENTO TIPO_PRIMITIVO TK_IDENTIFICADOR LISTA_DE_CAMPOS ']' EH_VETOR ';'{}
{
	if($9 != NULL)
		vector_set((dict_element**)&$2);
	declaration_set((dict_element**)&$2,stack);
};
DECL_VAR_GLOBAL: TIPO_USUARIO_STATIC TK_IDENTIFICADOR EH_VETOR ';'
{
	if($3 != NULL)
		vector_set((dict_element**)&$2);
	else
		variable_set((dict_element**)&$2);

	type_set((dict_element**)&$2,$1);
	declaration_set((dict_element**)&$2,stack);
	if(index_count != 0)
	{
		vector_dim_set((dict_element**)&$2, index_count, dim, 20);
		index_count = 0;
		int i =0;
	    for(i = 0; i<20; i++)
	    	dim[20] = 0;
	}
};

TIPO_USUARIO: 
TIPO_PRIMITIVO {$$ = $1;}
| TK_IDENTIFICADOR
{
//	$$ = $1;
//	declaration_set((dict_element**)&$1,stack);
};
TIPO_PRIMITIVO:
	  TK_PR_INT {$$ = IKS_INT;}
	| TK_PR_FLOAT {$$ = IKS_FLOAT;}
	| TK_PR_BOOL {$$ = IKS_BOOL;}
	| TK_PR_CHAR {$$ = IKS_CHAR;}
	| TK_PR_STRING {$$ = IKS_STRING;};

TIPO_USUARIO_CONST: TIPO_USUARIO {$$ = $1;}| TK_PR_CONST TIPO_USUARIO {$$ = $2;};
TIPO_USUARIO_STATIC: TIPO_USUARIO {$$ = $1;}| TK_PR_STATIC TIPO_USUARIO {$$ = $2;};
TIPO_USUARIO_CONST_STATIC: TIPO_USUARIO {$$ = $1;}| STATIC_CONST TIPO_USUARIO {$$ = $2;};

STATIC_CONST: TK_PR_CONST | TK_PR_STATIC | TK_PR_STATIC TK_PR_CONST;

LITERAL: 
	  TK_LIT_INT {$$ = astSymbolEntry((dict_element*)$1,AST_LITERAL);}
	| TK_LIT_FLOAT{$$ = astSymbolEntry((dict_element*)$1,AST_LITERAL);}
	| TK_LIT_TRUE{$$ = astSymbolEntry((dict_element*)$1,AST_LITERAL);}
	| TK_LIT_FALSE {$$ = astSymbolEntry((dict_element*)$1,AST_LITERAL);}
	| TK_LIT_CHAR {$$ = astSymbolEntry((dict_element*)$1,AST_LITERAL);}
	| TK_LIT_STRING{$$ = astSymbolEntry((dict_element*)$1,AST_LITERAL);};

EH_VETOR:
	'['TK_LIT_INT {dim[index_count] = vector_dim_size((dict_element*) $2); index_count++; }LISTA_DE_INDICES']' 
	{
		$$ = astSymbolEntry((dict_element*)$2,AST_LITERAL);
		tree_insert_checknull($$, $4);
	}
	|%empty {$$ = NULL;}
	;

LISTA_DE_INDICES:
	',' TK_LIT_INT {dim[index_count] = vector_dim_size((dict_element*) $2); index_count++; } LISTA_DE_INDICES
	{
		$$ = astSymbolEntry((dict_element*)$2,AST_LITERAL);
		tree_insert_checknull($$, $4);
	}
	|%empty {$$ = NULL;}
	;


LISTA_DE_CAMPOS:
	 ':' ENCAPSULAMENTO TIPO_PRIMITIVO TK_IDENTIFICADOR LISTA_DE_CAMPOS {declaration_set((dict_element**)&$4,stack); variable_set((dict_element**)&$4);}
	| %empty ;

ENCAPSULAMENTO: 
	  TK_PR_PRIVATE
	| TK_PR_PUBLIC
	| TK_PR_PROTECTED;

DECL_FUNC:
	TIPO_USUARIO_STATIC TK_IDENTIFICADOR '('
	{
		type_set((dict_element**)&$2,$1);
		function_set((dict_element**)&$2);
		declaration_set((dict_element**)&$2,stack);
		push(&stack);
	} TIPO_USUARIO_CONST TK_IDENTIFICADOR
	{
		type_set((dict_element**)&$6,$5);
		variable_set((dict_element**)&$6);
		declaration_set((dict_element**)&$6,stack);

		function_param_set((dict_element**)&$2, $5);
		function_actual_pointer = (dict_element*)$2;
		function_actual_type = $1;
	} LISTA_DE_PARAMETROS ')' '{' LISTA_DE_COMANDOS '}' 
	{
		pop(&stack);
		function_param_correct_order((dict_element**)&$2);
		$$ = tree_make_node(NULL);
		$$->value = createAstElement(AST_FUNCAO,(dict_element*)$2); 
		gv_declare_checknull(AST_FUNCAO, $$, "FUNCAO"); 
		tree_insert_checknull($$, $11);
		function_actual_type = 0;
		function_actual_pointer = NULL;
	}

	| TIPO_USUARIO_STATIC TK_IDENTIFICADOR '('')' 
	{
		type_set((dict_element**)&$2,$1);
		function_set((dict_element**)&$2);
		declaration_set((dict_element**)&$2,stack);
		function_actual_type = $1;
		push(&stack);
	} '{' LISTA_DE_COMANDOS '}' 
	{
		pop(&stack);
		$$ = tree_make_node(NULL);
		$$->value = createAstElement(AST_FUNCAO,NULL);
		gv_declare_checknull(AST_FUNCAO, $$, "FUNCAO");
		tree_insert_checknull($$,$7);
		function_actual_type = 0;
	};
LISTA_DE_PARAMETROS:
	',' TIPO_USUARIO_CONST TK_IDENTIFICADOR LISTA_DE_PARAMETROS
	{
		type_set((dict_element**)&$3,$2);
		variable_set((dict_element**)&$3);
		declaration_set((dict_element**)&$3,stack);
		function_param_set((dict_element**)&function_actual_pointer, $2);
	} 
	| %empty;

EXPRESSAO: 
	EXPRESSAO_ARIT 
	{
		$$ = $1;
	} 
	| EXPRESSAO_LOG {$$ = $1;} 
	| '-' EXPRESSAO {$$ = tree_make_node(NULL); gv_declare_checknull(AST_ARIM_INVERSAO, $$, NULL); $$->value = createAstElement(AST_ARIM_INVERSAO,NULL); tree_insert_checknull($$, $2);};

EXPRESSAO_ARIT:
	  	EXPRESSAO '+' TERMO 
	  	{ 
	  		ast_element* father = $$->value;
			ast_element* left = $1->value;
			ast_element* right = $3->value;

			coertion_check((dict_element**)&(father->entry),(dict_element*)left->entry, (dict_element*)right->entry);
			inherit_coertion((dict_element**)&(father->entry),(dict_element*)left->entry);

	  		$$ = tree_make_node(NULL);
	  		gv_declare_checknull(AST_ARIM_SOMA, $$, NULL);
	  		tree_insert_checknull($$, $1); tree_insert_checknull($$, $3);
	  		$$->value = createAstElement(AST_ARIM_SOMA,left->entry);
	  	}
	| EXPRESSAO '-' TERMO {
		ast_element* father = $$->value;
		ast_element* left = $1->value;
		ast_element* right = $3->value;

		coertion_check((dict_element**)&(father->entry),(dict_element*)left->entry, (dict_element*)right->entry);

		$$ = tree_make_node(NULL); gv_declare_checknull(AST_ARIM_SUBTRACAO, $$, NULL);  tree_insert_checknull($$, $1);
		tree_insert_checknull($$, $3); $$->value = createAstElement(AST_ARIM_SUBTRACAO,left->entry);
	}
	| TERMO 
	{
		$$ = $1;
	}
	;
	
TERMO: 
	  TERMO '*' FATOR 
	  { 
			ast_element* father = $$->value;
			ast_element* left = $1->value;
			ast_element* right = $3->value;

			coertion_check((dict_element**)&(father->entry),(dict_element*)left->entry, (dict_element*)right->entry);

			$$ = tree_make_node(NULL); gv_declare_checknull(AST_ARIM_MULTIPLICACAO, $$, NULL);
			tree_insert_checknull($$, $1); tree_insert_checknull($$, $3);
			$$->value = createAstElement(AST_ARIM_MULTIPLICACAO,left->entry);
		}
	| TERMO '/' FATOR 
	{
		ast_element* father = $$->value;
		ast_element* left = $1->value;
		ast_element* right = $3->value;

		coertion_check((dict_element**)&(father->entry),(dict_element*)left->entry, (dict_element*)right->entry);

		$$ = tree_make_node(NULL); gv_declare_checknull(AST_ARIM_DIVISAO, $$, NULL);
		tree_insert_checknull($$, $1); tree_insert_checknull($$, $3);
		$$->value = createAstElement(AST_ARIM_DIVISAO,left->entry);
	}
	| FATOR 
	{
		$$ = $1;
	}
	;

EXPRESSAO_LOG:
	 EXPRESSAO '<' EXPRESSAO { $$ = tree_make_node(NULL); gv_declare_checknull(AST_LOGICO_COMP_L, $$, NULL);  tree_insert_checknull($$, $1); tree_insert_checknull($$, $3); $$->value = createAstElement(AST_LOGICO_COMP_L,NULL);}
	|EXPRESSAO '>' EXPRESSAO { $$ = tree_make_node(NULL); gv_declare_checknull(AST_LOGICO_COMP_G, $$, NULL);  tree_insert_checknull($$, $1); tree_insert_checknull($$, $3); $$->value = createAstElement(AST_LOGICO_COMP_G,NULL);}
	|EXPRESSAO TK_OC_LE EXPRESSAO { $$ = tree_make_node(NULL); gv_declare_checknull(AST_LOGICO_COMP_LE, $$, NULL);  tree_insert_checknull($$, $1);  tree_insert_checknull($$, $3); $$->value = createAstElement(AST_LOGICO_COMP_LE,NULL); }
	|EXPRESSAO TK_OC_GE EXPRESSAO { $$ = tree_make_node(NULL); gv_declare_checknull(AST_LOGICO_COMP_GE, $$, NULL);  tree_insert_checknull($$, $1);  tree_insert_checknull($$, $3); $$->value = createAstElement(AST_LOGICO_COMP_GE,NULL);}
	|EXPRESSAO TK_OC_EQ EXPRESSAO { $$ = tree_make_node(NULL); gv_declare_checknull(AST_LOGICO_COMP_IGUAL, $$, NULL);  tree_insert_checknull($$, $1);  tree_insert_checknull($$, $3); $$->value = createAstElement(AST_LOGICO_COMP_IGUAL,NULL);}
	|EXPRESSAO TK_OC_NE EXPRESSAO { $$ = tree_make_node(NULL); gv_declare_checknull(AST_LOGICO_COMP_DIF, $$, NULL);  tree_insert_checknull($$, $1);  tree_insert_checknull($$, $3); $$->value = createAstElement(AST_LOGICO_COMP_DIF,NULL);}
	|EXPRESSAO TK_OC_AND EXPRESSAO{ $$ = tree_make_node(NULL); gv_declare_checknull(AST_LOGICO_E, $$, NULL);  tree_insert_checknull($$, $1);  tree_insert_checknull($$, $3); $$->value = createAstElement(AST_LOGICO_E,NULL);}
	|EXPRESSAO TK_OC_OR EXPRESSAO { $$ = tree_make_node(NULL); gv_declare_checknull(AST_LOGICO_OU, $$, NULL);  tree_insert_checknull($$, $1);  tree_insert_checknull($$, $3); $$->value = createAstElement(AST_LOGICO_OU,NULL);}
	|EXPRESSAO TK_OC_SL EXPRESSAO { $$ = tree_make_node(NULL); gv_declare_checknull(AST_SHIFT_LEFT, $$, NULL);  tree_insert_checknull($$, $1);  tree_insert_checknull($$, $3); $$->value = createAstElement(AST_SHIFT_LEFT,NULL);}
	|EXPRESSAO TK_OC_SR EXPRESSAO { $$ = tree_make_node(NULL); gv_declare_checknull(AST_SHIFT_RIGHT, $$, NULL);  tree_insert_checknull($$, $1);  tree_insert_checknull($$, $3); $$->value = createAstElement(AST_SHIFT_RIGHT,NULL);}
	;

	

FATOR:
	TK_IDENTIFICADOR 
	{
		declaration_test((dict_element**)&$1,stack);
		variable_test((dict_element*)$1);
		$$ = astSymbolEntry((dict_element*)$1,AST_IDENTIFICADOR);
		gv_declare_checknull(AST_IDENTIFICADOR, $$, "ID");

	}
	| TK_IDENTIFICADOR '['LISTA_DE_DIMENSOES']'
	{
		declaration_test((dict_element**)&$1,stack);
		vector_test((dict_element*)$1);

		//element_debug_print((dict_element*)expressao->entry); 
		//vector_index_check((dict_element*)expressao->entry);

		comp_tree_t* treeaux = astSymbolEntry((dict_element*)$1,AST_IDENTIFICADOR);
		gv_declare_checknull(AST_IDENTIFICADOR, treeaux, "ID");
		$$ = tree_make_node(NULL);
		gv_declare_checknull(AST_VETOR_INDEXADO, $$, NULL);

		$$->value = createAstElement(AST_VETOR_INDEXADO,(dict_element*)$1);
		tree_insert_checknull(treeaux, $3);
		tree_insert_checknull($$, treeaux);
		ast_element *expressao = treeaux->value;
	}
	| TK_LIT_CHAR {type_set((dict_element**)&$1, IKS_CHAR); $$ = astSymbolEntry((dict_element*)$1,AST_LITERAL);  gv_declare_checknull(AST_LITERAL, $$, "INT_CHAR");}
	| TK_LIT_STRING {type_set((dict_element**)&$1, IKS_STRING); $$ = astSymbolEntry((dict_element*)$1,AST_LITERAL);  gv_declare_checknull(AST_LITERAL, $$, "INT_STRING");}
	| TK_LIT_INT 
	{
		type_set((dict_element**)&$1, IKS_INT);
		$$ = astSymbolEntry((dict_element*)$1,AST_LITERAL);
		gv_declare_checknull(AST_LITERAL, $$, "INT_LIT");
		}
	| TK_LIT_FLOAT {type_set((dict_element**)&$1, IKS_FLOAT); $$ = astSymbolEntry((dict_element*)$1,AST_LITERAL); gv_declare_checknull(AST_LITERAL, $$, "FLOAT_LIT"); }
	| TK_LIT_TRUE {type_set((dict_element**)&$1, IKS_BOOL); $$ = astSymbolEntry((dict_element*)$1,AST_LITERAL); gv_declare_checknull(AST_LITERAL, $$, "TRUE_LIT");}
	| TK_LIT_FALSE {type_set((dict_element**)&$1, IKS_BOOL); $$ = astSymbolEntry((dict_element*)$1,AST_LITERAL); gv_declare_checknull(AST_LITERAL, $$, "FALSE_LIT");}
	| CHAMADA_FUNC 
	{
		ast_element* ast = $1->value;
		$$ = $1;
	}
	| '('EXPRESSAO')'{$$ = $2;}
	| '!' FATOR 
	{
		ast_element *fator = $2->value;
		boolean_test((dict_element*)fator->entry);

		$$ = tree_make_node(NULL); 
		gv_declare_checknull(AST_LOGICO_COMP_NEGACAO, $$, NULL); 
		$$->value = createAstElement(AST_ARIM_INVERSAO,NULL); 
		tree_insert_checknull($$, $2);
	}
	;

BLOCO_DE_COMANDOS:
	'{' {push(&stack);} LISTA_DE_COMANDOS'}'
	{
		$$ = tree_make_node(NULL);
		gv_declare_checknull(AST_BLOCO, $$, NULL);
		$$->value = createAstElement(AST_BLOCO,NULL);
		tree_insert_checknull($$,$3);
		pop(&stack);
	}
	;
LISTA_DE_COMANDOS: COMANDO ';' LISTA_DE_COMANDOS 
{
	if($1 != NULL)
	{
		$$ = $1;
	tree_insert_checknull($$,$3); 
	}
	else
		$$ = $3;
}
	| ';' LISTA_DE_COMANDOS {$$ = $2;}
	| TK_PR_CASE TK_LIT_INT ':' LISTA_DE_COMANDOS {$$ = $4;}
	| ENTRADA_SAIDA ';' LISTA_DE_COMANDOS
	|%empty {$$ = NULL;};

COMANDO:
 BLOCO_DE_COMANDOS {$$ = $1;} 
| DECLARACAO {$$ = $1;}
| CHAMADA_FUNC {$$ = $1;}
| ATRIBUICAO {$$ = $1;}
| SHIFT {$$ = $1;}
| RBC{$$ = NULL;}
| RETURN  {$$ = NULL;}
| IF {$$ = $1;}
| FOREACH {$$ = NULL;}
| FOR {$$ = NULL;}
| WHILE {$$ = $1;}
| SWITCH {$$ = NULL;}

;

DECLARACAO: 
	  TIPO_USUARIO_CONST_STATIC TK_IDENTIFICADOR EH_VETOR
	  {
	  	declaration_set((dict_element**)&$2,stack); $$= NULL;

	  	type_set((dict_element**)&$2,$1);
	  	variable_set((dict_element**)&$2);
	  }
	| TIPO_USUARIO_CONST_STATIC TK_IDENTIFICADOR TK_OC_LE TK_IDENTIFICADOR 
	{
		declaration_test((dict_element**)&$4,stack);

		type_set((dict_element**)&$2,$1);
		variable_set((dict_element**)&$2);
		declaration_set((dict_element**)&$2,stack);


		$$ = tree_make_node(NULL); 
		gv_declare_checknull(AST_ATRIBUICAO, $$, NULL); 
		$$->value = createAstElement(AST_ATRIBUICAO,NULL);
		comp_tree_t* treeaux = astSymbolEntry((dict_element*)$2,AST_IDENTIFICADOR);
 		gv_declare_checknull(AST_IDENTIFICADOR, treeaux, "ID");
		comp_tree_t* treeaux2 = astSymbolEntry((dict_element*)$4,AST_IDENTIFICADOR);
 		gv_declare_checknull(AST_IDENTIFICADOR, treeaux2, "ID");
		tree_insert_checknull($$, treeaux);
		tree_insert_checknull($$, treeaux2);

	}
	| TIPO_USUARIO_CONST_STATIC TK_IDENTIFICADOR TK_OC_LE LITERAL 
	{
		type_set((dict_element**)&$2,$1);
		variable_set((dict_element**)&$2);
		declaration_set((dict_element**)&$2,stack);

		$$ = tree_make_node(NULL); 
		gv_declare_checknull(AST_ATRIBUICAO, $$, NULL); 
		$$->value = createAstElement(AST_ATRIBUICAO,NULL);
		comp_tree_t* treeaux = astSymbolEntry((dict_element*)$2,AST_IDENTIFICADOR);
 		gv_declare_checknull(AST_IDENTIFICADOR, treeaux, "ID");
		tree_insert_checknull($$, treeaux);
		tree_insert_checknull($$, $4);
	}
;

CHAMADA_FUNC: 
	TK_IDENTIFICADOR 
	{
		declaration_test((dict_element**)&$1,stack);
		function_test((dict_element*)$1);
		param_period = true;
		param_cursor = param_list_get((dict_element*)$1);
	}
	'('ARGUMENTOS')' {
	param_count_check((dict_element*)$1,param_count);
	param_count = 0;
	param_period = false;
	comp_tree_t* treeaux = astSymbolEntry((dict_element*)$1,AST_IDENTIFICADOR);
	gv_declare_checknull(AST_LITERAL, treeaux, "ID");
	$$ = tree_make_node(NULL);
	$$->value = createAstElement(AST_CHAMADA_DE_FUNCAO, (dict_element*)$1);
	gv_declare_checknull(AST_CHAMADA_DE_FUNCAO, $$, NULL); 
	tree_insert_checknull($$, treeaux);
	if($4 != NULL)
	tree_insert_checknull($$, $4); 

	}

ARGUMENTOS: 
	LISTA_DE_ARGUMENTOS
	| %empty{$$ = NULL;};

	LISTA_DE_ARGUMENTOS: EXPRESSAO 
	{
		ast_element* expressao = $1->value;

		//$$ = $1;
		Ilist* present_cursor = param_cursor;
		if(param_period == true)
		{
			param_cursor = param_check((dict_element*)expressao->entry, (Ilist*)present_cursor);
			param_count++;
		}

	}',' LISTA_DE_ARGUMENTOS {$$=$1;  tree_insert_checknull($$,$4);}
	| EXPRESSAO 
	{
		$$=$1;

		ast_element* expressao = $1->value;
		Ilist* present_cursor = param_cursor;
		if(param_period == true)
		{
			param_cursor = param_check((dict_element*)expressao->entry, (Ilist*)present_cursor);
			param_count++;
		}
	}
	;


ATRIBUICAO: 

		 TERMO_AT '=' EXPRESSAO 
		 {
		 	ast_element* father = $$->value;
			ast_element* left = $1->value;
			ast_element* right = $3->value;

			//printf("\naddr:%p\n", right->entry);	
			//element_debug_print((dict_element*)right->entry);
			//element_debug_print((dict_element*)left->entry);

			coertion_check((dict_element**)&(father->entry),(dict_element*)left->entry, (dict_element*)right->entry);

		 	$$ = tree_make_node(NULL);
		 	gv_declare_checknull(AST_ATRIBUICAO, $$, NULL);
		 	$$->value = createAstElement(AST_ATRIBUICAO,NULL);
		 	tree_insert_checknull($$, $1);
		 	tree_insert_checknull($$, $3);
		 }
;


TERMO_AT: 
		TK_IDENTIFICADOR {
			declaration_test((dict_element**)&$1,stack);
			variable_test((dict_element*)$1);

			$$ = astSymbolEntry((dict_element*)$1,AST_IDENTIFICADOR);
			gv_declare_checknull(AST_IDENTIFICADOR, $$, "ID");
			}				
		| TK_IDENTIFICADOR '['LISTA_DE_DIMENSOES']'	 
		{
			declaration_test((dict_element**)&$1,stack);
			vector_test((dict_element*)$1);

			//ast_element *expressao = $3->value;
			//element_debug_print((dict_element*)expressao->entry);
			//vector_index_check((dict_element*)expressao->entry);
			 comp_tree_t* treeaux = astSymbolEntry((dict_element*)$1,AST_IDENTIFICADOR);
			 gv_declare_checknull(AST_IDENTIFICADOR, treeaux, "ID");
			 $$ = tree_make_node(NULL);
			 gv_declare_checknull(AST_VETOR_INDEXADO, $$, NULL);
			 $$->value = createAstElement(AST_VETOR_INDEXADO,(dict_element*)$1);
			 tree_insert_checknull($$, treeaux);
			 tree_insert_checknull(treeaux, $3);
		}
	 
	| TK_IDENTIFICADOR '!' TK_IDENTIFICADOR  {
		declaration_test((dict_element**)&$1,stack);
		declaration_test((dict_element**)&$3,stack);

		variable_test((dict_element*)$1);
		variable_test((dict_element*)$3);

			$$ = astSymbolEntry((dict_element*)$1,AST_IDENTIFICADOR);
			gv_declare_checknull(AST_IDENTIFICADOR, $$, "ID");

			tree_insert_checknull($$, astSymbolEntry((dict_element*)$3,AST_IDENTIFICADOR));
			}	
		;

	LISTA_DE_DIMENSOES: EXPRESSAO 
	{

	}',' LISTA_DE_DIMENSOES {$$=$1;  tree_insert_checknull($$,$4);}
	| EXPRESSAO 
	{
		$$=$1;
	}
	;

ENTRADA_SAIDA:  TK_PR_INPUT EXPRESSAO 
{
	ast_element* expressao = $2->value;

	input_test((dict_element*)expressao->entry);

	$$ = tree_make_node(NULL);
	gv_declare_checknull(AST_INPUT, $$, NULL);
	$$->value = createAstElement(AST_INPUT,NULL);
}
| TK_PR_OUTPUT LISTA_DE_EXPRESSOES 
{
	ast_element* expressao = $2->value;
	//element_debug_print((dict_element*)expressao->entry);
	output_test((dict_element*)expressao->entry);

	$$ = tree_make_node(NULL);
	gv_declare_checknull(AST_OUTPUT, $$, NULL);
	$$->value = createAstElement(AST_OUTPUT,NULL);
	tree_insert_checknull($$, $2);
}
;



SHIFT: 
	TK_IDENTIFICADOR TK_OC_SL TK_LIT_INT 
	{
		declaration_test((dict_element**)&$1,stack);
		variable_test((dict_element*)$1);

		$$ = tree_make_node(NULL); 
		gv_declare_checknull(AST_SHIFT_LEFT, $$, NULL); 
		$$->value = createAstElement(AST_SHIFT_LEFT,NULL);
	}
	| TK_IDENTIFICADOR TK_OC_SR TK_LIT_INT
	{
		declaration_test((dict_element**)&$1,stack);
		variable_test((dict_element*)$1);

		$$ = tree_make_node(NULL); gv_declare_checknull(AST_SHIFT_RIGHT, $$, NULL);
		$$->value = createAstElement(AST_SHIFT_RIGHT,NULL);
	};

RETURN: TK_PR_RETURN EXPRESSAO 
{ 
	ast_element* expressao = $2->value;
	return_test((dict_element*)expressao->entry, function_actual_type);
	$$ = tree_make_node(NULL);
	gv_declare_checknull(AST_RETURN, $$, NULL);
	tree_insert_checknull($$, $2); $$->value = createAstElement(AST_RETURN,NULL);
};

RBC: TK_PR_BREAK{}
| TK_PR_CONTINUE{}
;
IF:
	TK_PR_IF '('EXPRESSAO')' TK_PR_THEN COMANDO ';'
	 {
		$$ = tree_make_node(NULL); 
		gv_declare_checknull(AST_IF_ELSE, $$, NULL);
		tree_insert_checknull($$, $3);  
		tree_insert_checknull($$, $6); 
		tree_insert_checknull($$, NULL); 
		$$->value = createAstElement(50,NULL);
	}
	| TK_PR_IF '('EXPRESSAO')' TK_PR_THEN COMANDO ';' TK_PR_ELSE COMANDO ';'
	{
		$$ = tree_make_node(NULL);
		gv_declare_checknull(AST_IF_ELSE, $$, NULL); 
		tree_insert_checknull($$, $3); 
		tree_insert_checknull($$, $6); 
		tree_insert_checknull($$,$9); 
		$$->value = createAstElement(AST_IF_ELSE,NULL);
	}
	;

FOREACH: 
	TK_PR_FOREACH '('TK_IDENTIFICADOR ':' LISTA_DE_EXPRESSOES ')' COMANDO ';'
	{
		declaration_test((dict_element**)&$3,stack);
		variable_test((dict_element*)$3);
	};

LISTA_DE_EXPRESSOES: 
	EXPRESSAO { $$=$1; }
	| EXPRESSAO ',' LISTA_DE_EXPRESSOES {$$=$1;  tree_insert_checknull($$,$3);} 
	;

FOR: TK_PR_FOR '(' FOR_LISTA ':' EXPRESSAO ':' FOR_LISTA ')' COMANDO ';';

FOR_LISTA: COMANDO | COMANDO FOR_COMMA_GER | %empty ;

FOR_COMMA_GER: ',' COMANDO | ',' COMANDO FOR_COMMA_GER;

WHILE: TK_PR_WHILE '('EXPRESSAO')' TK_PR_DO COMANDO ';' { $$ = tree_make_node(NULL); gv_declare_checknull(AST_WHILE_DO, $$, NULL); tree_insert_checknull($$, $6); tree_insert_checknull($$, $3); $$->value = createAstElement(AST_WHILE_DO,NULL);}
	| TK_PR_DO COMANDO ';' TK_PR_WHILE '('EXPRESSAO')' { $$ = tree_make_node(NULL); gv_declare_checknull(AST_DO_WHILE, $$, NULL); tree_insert_checknull($$, $2); tree_insert_checknull($$, $6); $$->value = createAstElement(AST_DO_WHILE,NULL);}
	;

SWITCH: TK_PR_SWITCH '('EXPRESSAO')' COMANDO;




%%
