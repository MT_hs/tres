#include <stdlib.h>
#include <string.h>
#include "cc_misc.h"
#include "cc_dict.h"
#include "cc_tree.h"
#include "cc_ast.h"
#include "main.h"

extern char* yytext;
int start = 1;

extern int myLineNumber;
comp_stack_t *stack;
comp_tree_t *tree;
comp_dict_item_t *item;
dict_element *element;
int dim[20];
int escopo_global = 0; //zero quando global, maior que zero quando não global
unsigned long long rbss_offset = 0;
unsigned long long fp_offset = 0;

int comp_get_line_number (void)
{
  //implemente esta funÃ§Ã£o
  return myLineNumber;
}

void yyerror (char const *mensagem)
{
  fprintf (stderr, "Erro sintático na linha %d: \"%s\" inesperado \n", myLineNumber, yytext); 
}

void main_init (int argc, char **argv)
{
char str[20];
stack = stack_new();
tree = tree_new();

int i=0;
for(i =0 ; i<20; i++)
	dim[i]=0;
//dict_put(dict,"\0");
  //implemente esta funÃ§Ã£o com rotinas de inicializaÃ§Ã£o, se necessÃ¡rio
}

void main_finalize (void)
{
 int i = 0;
 ast_element* aux;

//dict_debug_print(stack->dict);

 	codeGen(tree);

 	aux = tree->value;
 	printf("%s",aux->code);

  for (i = 0; i < stack->dict->size ; i++) 	
    if (stack->dict->data[i]) 
    {
    	element = stack->dict->data[i]->value;
      	//cc_dict_etapa_2_print_entrada (dict->data[i]->key, element->line, element->token);
    }
 

   for (i = 0; i < stack->dict->size ; i++) 	
    {
	while (stack->dict->data[i])
        {
	item = stack->dict->data[i];          
	element = item->value;

	  if(element->token == SIMBOLO_LITERAL_STRING || element->token == SIMBOLO_IDENTIFICADOR)
          	free(element->literal.s);
	  free(element);
	  dict_remove(stack->dict,item->key);
	}
    }
//   dict_free(dict);

   tree_free(tree);
   //stack_free(stack);
   exit(IKS_SUCCESS);
}

void comp_print_table (void)
{
int i = 0;
  for (i = 0; i < stack->dict->size ; i++) 	
    if (stack->dict->data[i]) 
    {
    	element = stack->dict->data[i]->value;
      	cc_dict_etapa_2_print_entrada (stack->dict->data[i]->key, element->line, element->token);
    }
 
}
ast_element* createAstElement(int AST_TYPE , dict_element* element)
{
	ast_element* New_node_element = malloc(sizeof(ast_element));
	if(AST_TYPE == 50)
	{	//if sem else
		AST_TYPE = AST_IF_ELSE;
		New_node_element->semelse = true;
	}
	else
		New_node_element->semelse = false;


	New_node_element->AST_TYPE = AST_TYPE;
	New_node_element->entry = element;

	return New_node_element;
}

comp_tree_t* astSymbolEntry(dict_element* element, int AST_TYPE)
{
	comp_tree_t* symbol_entry;
	symbol_entry = tree_make_node(NULL); 
	symbol_entry->value = createAstElement(AST_TYPE,element);
	return symbol_entry;
}

void tree_insert_checknull(comp_tree_t *tree, comp_tree_t *node)
{
	if (node != NULL && tree!=NULL)
		tree_insert_node(tree, node);
	gv_connect_checknull(tree, node);
}

void gv_declare_checknull (const int tipo, const void *pointer, char* name)
{
	comp_tree_t* node= (comp_tree_t*)pointer;
	ast_element* ast_elem;
	dict_element* element;
	if (pointer != NULL)
	{
		ast_elem = node->value;
		if((tipo == AST_LITERAL || tipo == AST_IDENTIFICADOR || tipo == AST_FUNCAO) && ast_elem != NULL)
		{
			element = ast_elem->entry;
			if(tipo == AST_LITERAL)
				gv_declare(tipo, pointer, "literal");
			else
			if(tipo == AST_IDENTIFICADOR || tipo == AST_FUNCAO)
				gv_declare(tipo, pointer, "element->literal.s");
		}
		else
			gv_declare(tipo, pointer, name);
	}
}

void gv_connect_checknull (const void *p1, const void *p2)
{
	if(p1 != NULL && p2 != NULL)
	{
		gv_connect(p1, p2);
	}
}

comp_stack_t* stack_new()
{
	comp_stack_t* stack = malloc(sizeof(comp_stack_t));
	stack->dict = dict_new();
	stack->next = NULL;
	return stack;
}
void stack_free(comp_stack_t *stack)
{
	dict_free(stack->dict);
	free(stack);
}

void push(comp_stack_t** stack)
{
	comp_stack_t* _stack;
	comp_stack_t* aux;

	aux = *stack;
	comp_stack_t* stack_head = stack_new();

	*stack = stack_head;	
	_stack = *stack;
	_stack->next = aux;
	escopo_global++;
}
void pop(comp_stack_t** stack)
{
	comp_stack_t* _stack;
	_stack = *stack;

	comp_dict_t* dict = _stack->dict;

	if(_stack->next != NULL)
		*stack = _stack->next;

escopo_global--;
}

void declaration_set(dict_element** element, comp_stack_t* stack)
{
	dict_element* _element;
	dict_element* first;

	_element =  *element;
	bool loop = true;

	//tratamento da string key para poder consultar na tabela de símbolos
	//eh uma concatenacao entre o valor de token TK_IDENTIFICADOR(6) e o literal do elemento
	char* key = (char*)calloc(sizeof(_element->literal.s) + 2, sizeof(char));
	key[0] = _element->token + 48;
	strcpy(key+1,(char*)_element->literal.s);

	//consulta na tabela de símbolos e retorna o primeiro elemento
	first = (dict_element*)dict_get(stack->dict, key);

	if(first->declared == false)
	{
		_element->declared = true;
	
	//#ETAPA5
		if(escopo_global == 0)
		{
			_element->offset = rbss_offset;
			_element->offset_base = _element->offset;
			rbss_offset += _element->size;
			_element->global = true;
		}
		else
		{
			_element->offset = fp_offset;
			_element->offset_base = _element->offset;
			fp_offset += _element->size;
			_element->global = false;
		}

	}
	else
	{
		//no mesmo escopo
		printf("line %d: Double declaration of variable \"%s\".\n", _element->line, _element->literal.s);
		exit(IKS_ERROR_DECLARED);
	}
		free(key);
}

//declaration_test(dict_element* element)
void declaration_test(dict_element** element, comp_stack_t* stack)
{
	dict_element* first;
	dict_element* _element;
	_element = *element;
	bool loop = true;

	//tratamento da string key para poder consultar na tabela de símbolos
	//eh uma concatenacao entre o valor de token TK_IDENTIFICADOR(6) e o literal do elemento
	char* key = (char*)calloc(sizeof(_element->literal.s) + 2, sizeof(char));
	key[0] = _element->token + 48;
	strcpy(key+1,(char*)_element->literal.s);

	//consulta na tabela de símbolos e retorna o primeiro elemento
	first = (dict_element*)dict_get(stack->dict, key);

	while(loop == true)
	{
			if(first != NULL && first->declared == true)
				{
					loop=false;
				}
			else
			{
				if(stack->next != NULL)
				{
					stack = stack->next;
					first = (dict_element*)dict_get(stack->dict, key);
				}
				else
				{
					printf("line %d: Undeclared variable \"%s\".\n", _element->line, _element->literal.s);
					exit(IKS_ERROR_UNDECLARED);
				}
			}
	}
	_element->type = first->type;
	_element->variable = first->variable;
	_element->vector = first->vector;
	_element->function = first->function;
	_element->type = first->type;
	_element->coertion = first->coertion;
	_element->size = first->size;
	_element->type_size = first->type_size;
	_element->offset = first->offset;
	_element->global = first->global;
	_element->param_counter = first->param_counter;
	_element->INTlist = first->INTlist;
	_element->INTlist_tail = first->INTlist_tail;

	_element->dim = first->dim;
	int i = 0;
	for(i=0; i< 20; i++)
	    _element->dimsize[i] = first->dimsize[i];

	free(key);
	//printf("Variable %s declared \n",element->literal.s);
}

void variable_set(dict_element** element)
{
	dict_element* _element;
	_element = *element;
	_element->variable = true;
}

void variable_test(dict_element* element)
{
	if(element->function == true)
	{
		printf("line %d: \"%s\" should be a function.\n", element->line, element->literal.s);
		exit(IKS_ERROR_FUNCTION);
	}

	if(element->vector == true)
	{
		printf("line %d: \"%s\" should be a vector.\n", element->line, element->literal.s);
		exit(IKS_ERROR_VECTOR);
	}
}

void vector_set(dict_element** element)
{
	dict_element* _element;
	_element = *element;
	_element->vector = true;
	//printf("line %d: vetor: %s \n",_element->line, _element->literal.s);
}

void vector_test(dict_element* element)
{
	if(element->variable == true)
	{
		printf("line %d: \"%s\" should be a variable.\n", element->line, element->literal.s);
		exit(IKS_ERROR_VARIABLE);
	}

	if(element->function == true)
	{
		printf("line %d: \"%s\" should be a function.\n", element->line, element->literal.s);
		exit(IKS_ERROR_FUNCTION);
	}
}

int vector_dim_size(dict_element* element)
{
	return element->literal.i;
}

void vector_dim_set(dict_element** element, int dim, int* vec, int vec_size)
{
	int vector_size = 1;
	dict_element* _element;
	_element = *element;
	_element->dim = dim;

	int i =0;
	for(i = 0; i<dim; i++)
		{
			_element->dimsize[i] = vec[i];
			vector_size = (vector_size)*vec[i];
		}
	_element->size = _element->size*vector_size;

	if(escopo_global == 0)
		{
			rbss_offset-=_element->type_size;
			_element->offset = rbss_offset;
			rbss_offset += _element->size;
			_element->global = true;
		}
		else
		{
			rbss_offset-=_element->type_size;
			_element->offset = fp_offset;
			fp_offset += _element->size;
			_element->global = false;
		}
}
void function_set(dict_element** element)
{
	dict_element* _element;
	_element = *element;
	_element->function = true;
}

void function_test(dict_element* element)
{
	if(element->variable == true)
	{
		printf("line %d: \"%s\" should be a variable.\n", element->line, element->literal.s);
		exit(IKS_ERROR_VARIABLE);
	}

	if(element->vector == true)
	{
		printf("line %d: \"%s\" should be a vector.\n", element->line, element->literal.s);
		exit(IKS_ERROR_VECTOR);
	}
}

void type_set(dict_element** element, int type)
{
	dict_element* _element;
	_element = *element;
	_element->type = type;

	switch(type)
	{
		case IKS_INT:
			_element->size = 4;
			_element->type_size = 4;
			break;
		case IKS_FLOAT: 
			_element->size = 8;
			_element->type_size = 8;
			break;
		case IKS_CHAR: 
			_element->size = 1;
			_element->type_size = 1;
			break;
		case IKS_STRING: 
			_element->size = sizeof(_element->literal.s);
			break;
		case IKS_BOOL: 
			_element->size = 1;
			_element->type_size = 1;
			break;
	}
}

void coertion_check(dict_element** father_element, dict_element* left, dict_element* right)
{
	dict_element* _father_element;
	dict_element* _left;
	dict_element* _right;
	_father_element = *father_element;
	_left = left;
	_right = right;

	if(_left->coertion == 0)
		switch(_left->type)
		{
			case IKS_INT:
				switch(_right->type)
				{
					case IKS_INT:
					//_father_element->coertion = IKS_INT;
						break;
					case IKS_FLOAT:
						_father_element->coertion = IKS_FLOAT;
						break;
					case IKS_CHAR:
						printf("line %d: Cannot operate char with an integer.\n", _right->line);
						exit(IKS_ERROR_CHAR_TO_X);
						break;
					case IKS_STRING:
						printf("line %d: Cannot operate string with an integer.\n", _right->line);
						exit(IKS_ERROR_STRING_TO_X);
						break;
					case IKS_BOOL:
						_father_element->coertion = IKS_INT;
						break;
				}
				break;
			case IKS_FLOAT:
				switch(_right->type)
					{
						case IKS_INT:
							_father_element->coertion = IKS_FLOAT;
							break;
						case IKS_FLOAT:
							//_father_element->coertion = IKS_FLOAT;
							break;
						case IKS_CHAR:
							printf("line %d: Cannot operate char with a float.\n", _right->line);
							exit(IKS_ERROR_CHAR_TO_X);
							break;
						case IKS_STRING:
							printf("line %d: Cannot operate string with a float.\n", _right->line);
							exit(IKS_ERROR_STRING_TO_X);
							break;
						case IKS_BOOL:
							_father_element->coertion = IKS_FLOAT;
							break;
					}
				break;
			case IKS_CHAR:
				switch(_right->type)
					{
						case IKS_INT:
							printf("line %d: wrong type.\n", _right->line);
							exit(IKS_ERROR_WRONG_TYPE);
							break;
						case IKS_FLOAT:
							printf("line %d: wrong type.\n", _right->line);
							exit(IKS_ERROR_WRONG_TYPE);
							break;
						case IKS_CHAR:
							//_father_element->coertion = IKS_CHAR;
							break;
						case IKS_STRING:
							printf("line %d: Cannot operate string with a char.\n", _right->line);
							exit(IKS_ERROR_STRING_TO_X);
							break;
						case IKS_BOOL:
							printf("line %d: wrong type.\n", _right->line);
							exit(IKS_ERROR_WRONG_TYPE);
							break;
					}
					break;
			case IKS_STRING:
				switch(_right->type)
					{
						case IKS_INT: 
							printf("line %d: wrong type.\n",_right->line);
							exit(IKS_ERROR_WRONG_TYPE);
							break;
						case IKS_FLOAT: 
							printf("line %d: wrong type.\n", _right->line);
							exit(IKS_ERROR_WRONG_TYPE);
							break;
						case IKS_CHAR:
							printf("line %d: Cannot operate string with a char.\n", _right->line);
							exit(IKS_ERROR_CHAR_TO_X);
							break;
						case IKS_STRING:
							//_father_element->coertion = IKS_STRING;
							break;
						case IKS_BOOL:
							printf("line %d: wrong type.\n", _right->line);
							exit(IKS_ERROR_WRONG_TYPE);
							break;
					}
					break;
			case IKS_BOOL:
				switch(_right->type)
					{
						case IKS_INT:
							_father_element->coertion = IKS_INT;
							break;
						case IKS_FLOAT:
							_father_element->coertion = IKS_FLOAT;
							break;
						case IKS_CHAR:
						printf("line %d: Cannot operate bool with a char.\n", _right->line);
						exit(IKS_ERROR_CHAR_TO_X);
							break;
						case IKS_STRING:
						printf("line %d: Cannot operate bool with a char.\n", _right->line);
						exit(IKS_ERROR_STRING_TO_X);
							break;
						case IKS_BOOL:
							//_father_element->coertion = IKS_BOOL;
							break;
					}
					break;
		}
	else
		switch(_left->coertion)
		{
			case IKS_INT:
				switch(_right->type)
				{
					case IKS_INT:
					//_father_element->coertion = IKS_INT;
						break;
					case IKS_FLOAT:
						_father_element->coertion = IKS_FLOAT;
						break;
					case IKS_CHAR:
						printf("line %d: Cannot operate char with an integer.\n", _right->line);
						exit(IKS_ERROR_CHAR_TO_X);
						break;
					case IKS_STRING:
						printf("line %d: Cannot operate string with an integer.\n", _right->line);
						exit(IKS_ERROR_STRING_TO_X);
						break;
					case IKS_BOOL:
						_father_element->coertion = IKS_INT;
						break;
				}
				break;
			case IKS_FLOAT:
				switch(_right->type)
					{
						case IKS_INT:
							_father_element->coertion = IKS_FLOAT;
							break;
						case IKS_FLOAT:
							//_father_element->coertion = IKS_FLOAT;
							break;
						case IKS_CHAR:
							printf("line %d: Cannot operate char with a float.\n", _right->line);
							exit(IKS_ERROR_CHAR_TO_X);
							break;
						case IKS_STRING:
							printf("line %d: Cannot operate string with a float.\n", _right->line);
							exit(IKS_ERROR_STRING_TO_X);
							break;
						case IKS_BOOL:
							_father_element->coertion = IKS_FLOAT;
							break;
					}
				break;
			case IKS_CHAR:
				switch(_right->type)
					{
						case IKS_INT:
							printf("line %d: wrong type.\n", _right->line);
							exit(IKS_ERROR_WRONG_TYPE);
							break;
						case IKS_FLOAT:
							printf("line %d: wrong type.\n", _right->line);
							exit(IKS_ERROR_WRONG_TYPE);
							break;
						case IKS_CHAR:
							//_father_element->coertion = IKS_CHAR;
							break;
						case IKS_STRING:
							printf("line %d: Cannot operate string with a char.\n", _right->line);
							exit(IKS_ERROR_STRING_TO_X);
							break;
						case IKS_BOOL:
							printf("line %d: wrong type.\n", _right->line);
							exit(IKS_ERROR_WRONG_TYPE);
							break;
					}
					break;
			case IKS_STRING:
				switch(_right->type)
					{
						case IKS_INT: 
							printf("line %d: wrong type.\n", _right->line);
							exit(IKS_ERROR_WRONG_TYPE);
							break;
						case IKS_FLOAT: 
							printf("line %d: wrong type.\n", _right->line);
							exit(IKS_ERROR_WRONG_TYPE);
							break;
						case IKS_CHAR:
							printf("line %d: Cannot operate string with a char.\n", _right->line);
							exit(IKS_ERROR_CHAR_TO_X);
							break;
						case IKS_STRING:
							//_father_element->coertion = IKS_STRING;
							break;
						case IKS_BOOL:
							printf("line %d: wrong type.\n", _right->line);
							exit(IKS_ERROR_WRONG_TYPE);
							break;
					}
					break;
			case IKS_BOOL:
				switch(_right->type)
					{
						case IKS_INT:
							_father_element->coertion = IKS_INT;
							break;
						case IKS_FLOAT:
							_father_element->coertion = IKS_FLOAT;
							break;
						case IKS_CHAR:
						printf("line %d: Cannot operate bool with a char.\n", _right->line);
						exit(IKS_ERROR_CHAR_TO_X);
							break;
						case IKS_STRING:
						printf("line %d: Cannot operate bool with a char.\n", _right->line);
						exit(IKS_ERROR_STRING_TO_X);
							break;
						case IKS_BOOL:
							//_father_element->coertion = IKS_BOOL;
							break;
					}
					break;
		}
}

void inherit_coertion(dict_element** father, dict_element* son)
{
	dict_element* _father;
	_father = *father;

	_father->coertion = son->coertion;
	_father->type = son->type;
}

void vector_index_check(dict_element* element)
{

if(element->type != IKS_BOOL || element->coertion != 0)
{
	if(element->type == IKS_CHAR)
	{
		printf("line %d: Index type cannot should be a number\n", element->line);
		exit(IKS_ERROR_CHAR_TO_X);
	}
	if(element->type == IKS_STRING)
	{
		printf("line %d: Index type cannot should be a number\n", element->line);
		exit(IKS_ERROR_STRING_TO_X);
	}
	if(element->type != IKS_INT && element->coertion != IKS_INT)
	{
		printf("line %d: Wrong index type\n", element->line);
		exit(IKS_ERROR_WRONG_TYPE);
	}
	if(element->coertion != IKS_INT && element->coertion != 0)
	{
		printf("line %d: Wrong index type\n", element->line);
		exit(IKS_ERROR_WRONG_TYPE);
	}
}
}
void return_test(dict_element* element, int function_type)
{
	if(function_type == IKS_STRING || function_type == IKS_CHAR || element->type == IKS_CHAR || element->type == IKS_STRING)
	{
		if(element->type != function_type)
		{
			printf("line %d: Wrong return type\n",element->line);
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}
	}
}

void function_param_set(dict_element** element, int type)
{
	dict_element* _element;
	 _element = *element;
	if(_element->INTlist == NULL)
	{
		_element->INTlist = malloc(sizeof(Ilist));
		_element->INTlist_tail = _element->INTlist;
		_element->INTlist->type = type;
		_element->INTlist->next = NULL;
		_element->param_counter++;
//		printf(" | Head:%d | T:%d\n", _element->INTlist_tail, _element->INTlist_tail->type);
	}
	else
	{
		_element->INTlist_tail->next = malloc(sizeof(Ilist));
		_element->INTlist_tail = _element->INTlist_tail->next;
		_element->INTlist_tail->type = type;
		_element->INTlist_tail->next = NULL;
		_element->param_counter++;
//		printf(" | Pointer:%d | T:%d\n", _element->INTlist_tail, _element->INTlist_tail->type);
	}
}

void function_param_test()
{

}

void function_param_debug(dict_element* element)
{
	Ilist* test;
	int i = 1;
	test = element->INTlist;
	while(test != NULL)
	{
//		printf("%d| Pointer:%d | T:%d\n",i , test, test->type);
		test = test->next;
		i++;
	}
}

void function_param_correct_order(dict_element** element)
{
	dict_element* _element;
	int i;
	int aux;
	int count = 1;
	Ilist* _cursor;
	_element = *element;

	_cursor = _element->INTlist;

	if(_cursor->next != NULL)
		_cursor = _cursor->next;

	while(_cursor->next != NULL)
	{
		_cursor = _cursor->next;
		count++;
	}
if(count > 1)
	{
		_cursor = _element->INTlist->next;
		while(count != 0)
		{
			for(i = 1; i < count; i++)
			{
				aux = _cursor->type;
				_cursor->type = _cursor->next->type;
				_cursor->next->type = aux;
				_cursor = _cursor->next;
			}
			count--;
			_cursor = _element->INTlist->next;
		}
	}
}

Ilist* param_list_get(dict_element* element)
{
	return element->INTlist;
}

Ilist* param_check(dict_element* element, Ilist* cursor)
{
	if(cursor != NULL)
	{
		if(element->type == IKS_STRING || cursor->type == IKS_STRING || element->type == IKS_CHAR || cursor->type == IKS_CHAR)
		{
			if(element->type != cursor->type)
			{
				printf("line %d: Wrong type arguments at function call\n", element->line);
				exit(IKS_ERROR_WRONG_TYPE_ARGS);
			}
		}
		return (cursor->next)? cursor->next : NULL;
	}
	else
		return NULL;
}

void param_count_check(dict_element* declared, int called)
{
	//printf("declared: %d | called: %d\n",declared->param_counter,called);
	if(declared->param_counter > (called))
	{
		printf("line %d: Missing arguments at function call\n",declared->line);
		exit(IKS_ERROR_MISSING_ARGS);
	}
	if(declared->param_counter < (called))
	{
		printf("line %d: Excess of arguments at function call\n",declared->line);
		exit(IKS_ERROR_EXCESS_ARGS);
	}
}
void element_debug_print(dict_element* element)
{

	if(element->function == true)
	{
		puts("	FUNTION");
		printf("name: %s\n",element->literal.s);
	}
	else
	if(element->vector == true)
	{
		puts("	VECTOR");
		printf("name: %s\n",element->literal.s);
	}
	else
	if(element->variable == true)
	{
		puts("	VARIABLE");
		printf("name: %s\n",element->literal.s);
	}
	else
		puts("	NULL TYPE");

	printf("%p| ",element);
	if(element->declared == true)
		puts("declared: YES");
	else
		puts("declared: NO");

	printf("type: %d | ",element->type);
	printf("coertion: %d\n",element->coertion);
	printf("line: %d | ", element->line);
	printf("offset: %llu\n", element->offset);
	printf("size: %llu | ", element->size);
	printf("type_size: %d", element->type_size);
	puts("");
	puts("************************");
}

void input_test(dict_element* element)
{
	if(element->variable == 0)
		exit(IKS_ERROR_WRONG_PAR_INPUT);
	if(element->type == IKS_STRING)
		exit(IKS_ERROR_WRONG_PAR_INPUT);
	if(element->type == IKS_CHAR)
		exit(IKS_ERROR_WRONG_PAR_INPUT);
}
void output_test(dict_element* element)
{
	if(element->variable == 0)
	{
		//if(element->literal.c != 0)
	}
	if(element->type == IKS_CHAR)
		exit(IKS_ERROR_WRONG_PAR_OUTPUT); //parametro nao e literal string ou expressao
}

void boolean_test(dict_element* element)
{
	if(element->type != IKS_BOOL)
		exit(IKS_ERROR_WRONG_TYPE);
	if(element->coertion != 0 && element->coertion != 5)
		exit(IKS_ERROR_WRONG_TYPE);
}

void codeGen(comp_tree_t* tree_a)
{
	int i;
	comp_tree_t* son;
	if (tree_a->childnodes != 0)
		son = tree_a->first;
	for (i=0; i< tree_a->childnodes; i++)
		{
			codeGen(son);
			son = son->next;
		}
	ast_element* aux;

	if(tree_a->value != NULL)
	{
		aux = tree_a->value;
		switch (aux->AST_TYPE)
		{
			case AST_PROGRAMA: codeProg(tree_a); break;
			case AST_FUNCAO: codeFunc(tree_a); break;
			case AST_IF_ELSE: codeIf(tree_a); break;
			case AST_DO_WHILE: codeDoWhile(tree_a); break;
			case AST_WHILE_DO: codeWhileDo(tree_a); break;
			case AST_BLOCO: codeBlock(tree_a); break;
			case AST_LITERAL: codeLit(tree_a); break;
			case AST_IDENTIFICADOR: codeIdent(tree_a); break;
			case AST_ARIM_SOMA:
			case AST_ARIM_SUBTRACAO:
			case AST_ARIM_MULTIPLICACAO:
			case AST_ARIM_DIVISAO: codeArim(tree_a); break;
			case AST_ATRIBUICAO: codeAtrib(tree_a); break;
			case AST_ARIM_INVERSAO:  codeInv(tree_a); break;
			case AST_LOGICO_E:  codeAnd(tree_a); break;
			case AST_LOGICO_OU: codeOr(tree_a); break;
			case AST_LOGICO_COMP_DIF: 
			case AST_LOGICO_COMP_IGUAL: 
			case AST_LOGICO_COMP_LE: 
			case AST_LOGICO_COMP_GE: 
			case AST_LOGICO_COMP_L:     
			case AST_LOGICO_COMP_G: codeComp(tree_a); break;
			case AST_LOGICO_COMP_NEGACAO: codeNot(tree_a); break;
			case AST_VETOR_INDEXADO: codeVec(tree_a); break;
			default:
				break;

		}
	}
}

void codeArim(comp_tree_t* tree_a)
{
	ast_element* aux;
	ast_element* aux_left;
	ast_element* aux_right;

	aux_left = tree_a->first->value;
	aux_right = tree_a->first->next->value;
	aux = tree_a->value;

	char* operation_string;
	operation_string = malloc(50*sizeof(char));
	
	newRegister(aux->reg);

		switch (aux->AST_TYPE)
		{
			case AST_ARIM_SOMA:
				sprintf(operation_string,"add %s, %s => %s\n", aux_left->reg, aux_right->reg, aux->reg);
				strcpy(aux->code ,aux_left->code);
				strcat(aux->code, aux_right->code);
				strcat(aux->code,operation_string);
				break;
			case AST_ARIM_SUBTRACAO:
				sprintf(operation_string,"sub %s, %s => %s\n", aux_left->reg, aux_right->reg, aux->reg);
				strcpy(aux->code ,aux_left->code);
				strcat(aux->code, aux_right->code);
				strcat(aux->code,operation_string);
				break;
			case AST_ARIM_MULTIPLICACAO:
				sprintf(operation_string,"mult %s, %s => %s\n", aux_left->reg, aux_right->reg, aux->reg);
				strcpy(aux->code ,aux_left->code);
				strcat(aux->code, aux_right->code);
				strcat(aux->code,operation_string);
				break;
			case AST_ARIM_DIVISAO:
				sprintf(operation_string,"div %s, %s => %s\n", aux_left->reg, aux_right->reg, aux->reg);
				strcpy(aux->code ,aux_left->code);
				strcat(aux->code, aux_right->code);
				strcat(aux->code,operation_string);
				break;
			}

	/*aux = tree_a->first->value;
	if (aux->AST_TYPE == AST_IDENTIFICADOR)
		printf(aux->local);*/
}

void codeAtrib(comp_tree_t* tree_a)
{
	ast_element* aux;
	ast_element* aux_left;
	ast_element* aux_right;
	ast_element* aux_last;

	aux_left = tree_a->first->value;
	aux_right = tree_a->first->next->value;
	aux = tree_a->value;
	aux_last = tree_a->last->value;

	char* operation_string;
	operation_string = malloc(50*sizeof(char));

	if(aux_left->entry->global == true)
	{
		sprintf(operation_string, "storeAI %s => rbss, %llu\n", aux_right->reg, aux_left->entry->offset);
	}
	else
	{
		sprintf(operation_string, "storeAI %s => rarp, %llu\n", aux_right->reg, aux_left->entry->offset);
	}

	strcpy(aux->code, aux_right->code);	
	strcat(aux->code, operation_string);

	if(tree_a->childnodes == 3)
		strcat(aux->code, aux_last->code);
	
}


void codeProg(comp_tree_t* tree_a)
{
	ast_element* aux = tree_a->value;
	ast_element* aux_son = tree_a->first->value;
	strcpy(aux->code, aux_son->code);

}
void codeFunc(comp_tree_t* tree_a)
{
	ast_element* aux = tree_a->value;
	ast_element* aux_son;

	if(tree_a->first != NULL)
		aux_son = tree_a->first->value;
	if(aux_son != NULL)
		strcpy(aux->code, aux_son->code);
}
void codeIf(comp_tree_t* tree_a)
{
	ast_element* aux;
	ast_element* aux_left;
	ast_element* aux_right;
	ast_element* aux_right_next;
	ast_element* aux_last;

	aux = tree_a->value;
	aux_left = tree_a->first->value;
	aux_right = tree_a->first->next->value;
	if(tree_a->childnodes > 2)
		aux_right_next = tree_a->first->next->next->value;

	aux_last = tree_a->last->value;

	char* operation_string;
	
	operation_string = malloc(50*sizeof(char));

	char labelTrue[10];
	char labelFalse[10];
	char labelAfter[10];	


	
	newLabel(labelFalse);
	newLabel(labelTrue);
	newLabel(labelAfter);


	sprintf(operation_string, "cbr %s -> %s, %s\n%s: nop\n",aux_left->reg,labelTrue,labelFalse,labelTrue);

	if(tree_a->childnodes == 4) //se for if com else e não for o ultimo comando
	{
	strcpy(aux->code, aux_left->code);
	strcat(aux->code, operation_string);
	strcat(aux->code, aux_right->code);
	strcat(aux->code, "jumpI -> ");
	strcat(aux->code, labelAfter);
	strcat(aux->code, "\n");
	strcat(aux->code, labelFalse);
	strcat(aux->code, ": nop\n");
	strcat(aux->code, aux_right_next->code);
	strcat(aux->code, labelAfter);
	strcat(aux->code, ": nop\n");
	strcat(aux->code, aux_last->code);
	}
	else if(tree_a->childnodes == 3)
	{
		if(aux->semelse == false)
		{
			strcpy(aux->code, aux_left->code);
			strcat(aux->code, operation_string);
			strcat(aux->code, aux_right->code);
			strcat(aux->code, "jumpI -> ");
			strcat(aux->code, labelAfter);
			strcat(aux->code, "\n");
			strcat(aux->code, labelFalse);
			strcat(aux->code, ": nop\n");
			strcat(aux->code, aux_right_next->code);
			strcat(aux->code, labelAfter);
			strcat(aux->code, ": nop\n");
		}
		else
		{
			strcpy(aux->code, aux_left->code);
			strcat(aux->code, operation_string);
			strcat(aux->code, aux_right->code);
			strcat(aux->code, "jumpI -> ");
			strcat(aux->code, labelAfter);
			strcat(aux->code, "\n");
			strcat(aux->code, labelFalse);
			strcat(aux->code, ": nop\n");
			strcat(aux->code, labelAfter);
			strcat(aux->code, ": nop\n");
			strcat(aux->code, aux_last->code);
		}
	}
	else
	{
		strcpy(aux->code, aux_left->code);
		strcat(aux->code, operation_string);
		strcat(aux->code, aux_right->code);
		strcat(aux->code, "jumpI -> ");
		strcat(aux->code, labelAfter);
		strcat(aux->code, "\n");
		strcat(aux->code, labelFalse);
		strcat(aux->code, ": nop\n");
		strcat(aux->code, labelAfter);
		strcat(aux->code, ": nop\n");
	}

	
}

void codeDoWhile(comp_tree_t* tree_a)
{
	ast_element* aux;
	ast_element* aux_left;
	ast_element* aux_right;
	ast_element* aux_last;

	aux = tree_a->value;
	aux_left = tree_a->first->value;
	aux_right = tree_a->first->next->value;
	aux_last = tree_a->last->value;

	char labelStart[10];
	char labelBranch[10];
	
	newLabel(labelStart);
	newLabel(labelBranch);
	
	char* operation_string1;
	char* operation_string2;
	
	operation_string1 = malloc(50*sizeof(char));
	operation_string2 = malloc(50*sizeof(char));

	//Do
	sprintf(operation_string1, "%s: nop\n", labelStart);
	//While
	sprintf(operation_string2, "cbr %s -> %s, %s\n%s: nop\n",aux_right->reg,labelStart,labelBranch,labelBranch);

	strcpy(aux->code, operation_string1);
	strcat(aux->code, aux_left->code);
	strcat(aux->code, aux_right->code);


	strcat(aux->code, operation_string2);

	if(tree_a->childnodes == 3)
		strcat(aux->code, aux_last->code);

}
void codeWhileDo(comp_tree_t* tree_a)
{
	ast_element* aux;
	ast_element* aux_left;
	ast_element* aux_right;
	ast_element* aux_last;

	aux = tree_a->value;
	aux_left = tree_a->first->value;
	aux_right = tree_a->first->next->value;
	aux_last = tree_a->last->value;

	char labelStart[10];
	char labelBranch[10];
	char labelTrue[10];
	
	newLabel(labelStart);
	newLabel(labelBranch);
	newLabel(labelTrue);

	char* operation_string1;
	char* operation_string2;
	char* operation_string3;
	char* operation_string4;

	operation_string1 = malloc(50*sizeof(char));
	operation_string2 = malloc(50*sizeof(char));
	operation_string3 = malloc(50*sizeof(char));
	operation_string4 = malloc(50*sizeof(char));

	//Start
	sprintf(operation_string1, "%s: nop\n", labelStart);
	//Test
	sprintf(operation_string2, "cbr %s -> %s, %s\n%s: nop\n",aux_right->reg,labelTrue,labelBranch, labelTrue);
	//Branch
	sprintf(operation_string3, "jumpI -> %s\n", labelStart);
	//End
	sprintf(operation_string4, "%s: nop\n", labelBranch);
	
	strcpy(aux->code, operation_string1);
	strcat(aux->code, aux_right->code);
	strcat(aux->code, operation_string2);

	strcat(aux->code, aux_left->code);

	strcat(aux->code, operation_string3);
	strcat(aux->code, operation_string4);

	if(tree_a->childnodes == 3)
		strcat(aux->code, aux_last->code);
}
void codeBlock(comp_tree_t* tree_a)
{
	ast_element* aux = tree_a->value;
	ast_element* aux_son = tree_a->first->value;
	strcpy(aux->code, aux_son->code);
}
void codeLit(comp_tree_t* tree_a)
{
	ast_element* aux;
	aux = tree_a->value;
	dict_element* element = aux->entry;

	newRegister(aux->reg);

	switch(element->type)
	{
		case IKS_INT:	
			sprintf(aux->code, "loadI %d => %s \n", element->literal.i, aux->reg);
			break;
		case IKS_BOOL:
			if(element->literal.b == 1)
		 		sprintf(aux->code, "loadI 1 => %s \n", aux->reg);
		 	else
		 		sprintf(aux->code, "loadI 0 => %s \n", aux->reg);
		 	break;
		case IKS_FLOAT:
		 	sprintf(aux->code, "loadI %f => %s \n", element->literal.f, aux->reg);
		 	break;
	}
}
void codeIdent(comp_tree_t* tree_a)
{
	ast_element* aux;
	aux = tree_a->value;
	dict_element* element = aux->entry;

	newRegister(aux->reg);

	if(element->global == true)
		sprintf(aux->code,"loadAI rbss, %llu => %s\n",element->offset, aux->reg);
	else
		sprintf(aux->code,"loadAI fp, %llu => %s\n",element->offset, aux->reg);
}

void codeInv(comp_tree_t* tree_a)
{
	ast_element* aux;
	ast_element* aux_left;
	ast_element* aux_right;

	aux = tree_a->value;
	aux_left = tree_a->first->value;
	aux_right = tree_a->first->next->value;

	dict_element* element = aux->entry;

	newRegister(aux->reg);

	sprintf(aux->code, "rsubI %s, 0 => %s\n", aux->reg, aux->reg);

}
void codeAnd(comp_tree_t* tree_a)
{
	ast_element* aux;
	ast_element* aux_left;
	ast_element* aux_right;

	aux = tree_a->value;
	aux_left = tree_a->first->value;
	aux_right = tree_a->first->next->value;

	dict_element* element = aux->entry;

	newRegister(aux->reg);

	char* operation_string;
	operation_string = malloc(50*sizeof(char));

	sprintf(operation_string,"add %s, %s => %s\n", aux_left->reg, aux_right->reg, aux_left->reg);
	//sprintf(operation_string, "i2i %s => %s\ncbr %s => %s, %s\n%s: nop\n",);
	strcpy(aux->code ,aux_left->code);
	strcat(aux->code, aux_right->code);
	strcat(aux->code,operation_string);

}
void codeOr(comp_tree_t* tree_a)
{
	ast_element* aux;
	ast_element* aux_left;
	ast_element* aux_right;

	aux = tree_a->value;
	aux_left = tree_a->first->value;
	aux_right = tree_a->first->next->value;

	dict_element* element = aux->entry;

	newRegister(aux->reg);

	char* operation_string;
	operation_string = malloc(50*sizeof(char));

	sprintf(operation_string,"or %s, %s => %s\n", aux_left->reg, aux_right->reg, aux_left->reg);
	//sprintf(operation_string, "i2i %s => %s\ncbr %s => %s, %s\n%s: nop\n",);
	strcpy(aux->code ,aux_left->code);
	strcat(aux->code, aux_right->code);
	strcat(aux->code,operation_string);
}
void codeComp(comp_tree_t* tree_a)
{
	ast_element* aux;
	ast_element* aux_left;
	ast_element* aux_right;
	ast_element* aux_last;

	aux = tree_a->value;
	aux_left = tree_a->first->value;
	aux_right = tree_a->first->next->value;
	aux_last = tree_a->last->value;
	
	char* operation_string;
	operation_string = malloc(50*sizeof(char));

	newRegister(aux->reg);

	switch(aux->AST_TYPE)
	{
			break;
		case AST_LOGICO_COMP_IGUAL: 
			sprintf(operation_string, "cmp_EQ %s, %s -> %s\n", aux_left->reg, aux_right->reg, aux->reg);
			break;
		case AST_LOGICO_COMP_L:
			sprintf(operation_string, "cmp_LT %s, %s -> %s\n", aux_left->reg, aux_right->reg, aux->reg);
			break;
		case AST_LOGICO_COMP_G:
			sprintf(operation_string, "cmp_GT %s, %s -> %s\n", aux_left->reg, aux_right->reg, aux->reg);
			break;
		case AST_LOGICO_COMP_LE:
			sprintf(operation_string, "cmp_LE %s, %s -> %s\n", aux_left->reg, aux_right->reg, aux->reg);
			break; 
		case AST_LOGICO_COMP_GE:
			sprintf(operation_string, "cmp_GE %s, %s -> %s\n", aux_left->reg, aux_right->reg, aux->reg); 
			break;     
		case AST_LOGICO_COMP_DIF: 
			sprintf(operation_string, "cmp_NE %s, %s -> %s\n", aux_left->reg, aux_right->reg, aux->reg);
	}

	strcpy(aux->code, aux_left->code);
	strcat(aux->code, aux_right->code);
	strcat(aux->code, operation_string);
}
void codeNot(comp_tree_t* tree_a)
{
	ast_element* aux;
	ast_element* aux_left;

	aux = tree_a->value;
	aux_left = tree_a->first->value;

	dict_element* element = aux->entry;

	newRegister(aux->reg);

	char* operation_string;
	operation_string = malloc(50*sizeof(char));

	sprintf(operation_string,"xorI %s, 1 => %s\n", aux_left->reg, aux->reg);
	//sprintf(operation_string, "i2i %s => %s\ncbr %s => %s, %s\n%s: nop\n",);
	strcpy(aux->code ,aux_left->code);
	strcat(aux->code,operation_string);

}

void newLabel (char* dest)

{
	static int count = 0;
	char aux[10];
	sprintf(aux, "%d", count);
	strcpy(dest, "L");
	strcat(dest, aux);
	count++;
}

void newRegister (char* dest)
{
	static int count = 0;
	char aux[10];
	sprintf(aux, "%d", count);
	strcpy(dest, "r");
	strcat(dest, aux);
	count++;
}

unsigned long long matrixAddressCalc(dict_element* element, int* vec)
{
	unsigned long long temp = 0;
	int i = 0;


	temp = vec[0];
	if(element->dim >1)
	for(i=1; i<element->dim; i++)
		{temp = temp * element->dimsize[i] + vec[i];}

	return temp*(element->type_size);
}

void codeVec(comp_tree_t* tree_a)
{
		ast_element* aux;
	aux = tree_a->first->value;

	int d=0;
	int vec[20];
	unsigned long long offset = 0;

	dict_element* element = aux->entry;

	ast_element* ast_aux;
	comp_tree_t* tree_walker;

	newRegister(aux->reg);


	tree_walker = tree_a->first->first; //first dimension
	ast_aux = tree_walker->value;

	for(d=0; d < element->dim ; d++)
	{
		vec[d] = ast_aux->entry->literal.i;
		if(tree_walker->first != NULL)
		{	
			tree_walker = tree_walker->first;
			ast_aux = tree_walker->value;
		}
	}
	element->offset = element->offset_base;
	offset = matrixAddressCalc(element,vec);
	offset += element->offset;
	element->offset = offset;
	if(element->global == true)
		{sprintf(aux->code,"loadAI rbss, %llu => %s\n",offset, aux->reg);}
	else
		sprintf(aux->code,"loadAI fp, %llu => %s\n",offset, aux->reg);

}

