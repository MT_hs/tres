#ifndef __MISC_H
#define __MISC_H
#include <stdio.h>
#include "cc_tree.h"
#include "cc_dict.h"
#include "cc_gv.h"

#define IKS_INT		1
#define IKS_FLOAT 	2
#define IKS_CHAR 	3
#define IKS_STRING 	4
#define IKS_BOOL 	5

#define IKS_SUCCESS           0 //caso nenhumao houver nenhum tipo de erro

/*Verificacao de declaracoes*/
#define IKS_ERROR_UNDECLARED  1 //identificador nao declarado
#define IKS_ERROR_DECLARED    2 //identificador ja declarado

/*Uso correto de identificadores*/
#define IKS_ERROR_VARIABLE    3 //identificador deve ser utilizado como variavel
#define IKS_ERROR_VECTOR      4 //identificador deve ser utilizado como vetor
#define IKS_ERROR_FUNCTION    5 //identificador deve ser utilizado como funcao

/*Tipos e tamanho de dados*/
#define IKS_ERROR_WRONG_TYPE  6 //tipos incompat́ıveis
#define IKS_ERROR_STRING_TO_X 7 //coercao imposśıvel do tipo string
#define IKS_ERROR_CHAR_TO_X   8 //coercao imposśıvel do tipo char

/*Argumentos e parametros*/
#define IKS_ERROR_MISSING_ARGS    9  //faltam argumentos
#define IKS_ERROR_EXCESS_ARGS     10 //sobram argumentos
#define IKS_ERROR_WRONG_TYPE_ARGS 11 //argumentos incompatıveis

/* Verificacao de tipos em comando */
#define IKS_ERROR_WRONG_PAR_INPUT  12 //parametro nao e identificador
#define IKS_ERROR_WRONG_PAR_OUTPUT 13 //parametro nao e literal string ou expressao
#define IKS_ERROR_WRONG_PAR_RETURN 14 //parametro nao e expressao compatıvel com tipo do retorno

extern int myLineNumber;
int getLineNumber (void);
int comp_get_line_number (void);
void yyerror (char const *mensagem);
void main_init (int argc, char **argv);
void main_finalize (void);
void comp_print_table (void);
void gv_connect_checknull (const void *p1, const void *p2);
void gv_declare_checknull (const int tipo, const void *pointer, char *name);
void tree_insert_checknull(comp_tree_t *tree, comp_tree_t *node);

typedef enum boolean
{
true,
false
} bool;

typedef union value 
{
	bool b;
	int i;
	float f;
	char c;
	char* s;
} element_value;

typedef struct _Ilist {
	int type;
	struct _Ilist *next;
} Ilist;

typedef struct _dict_element
{
	bool declared; // 1.2 done
	bool vector;   // 1.3 todo
	bool function;  // 1.3
	bool variable;
	bool global;
	unsigned long long offset_base;
	unsigned long long offset;
	int type;      // 1.4 IKS_INT IKS_FLOAT IKS_CHAR IKS_STRING IKS_BOOL
	int coertion;  // 1.5 IKS_INT IKS_FLOAT IKS_BOOL
	unsigned long long size;      // 1.4
	int type_size;
	int line;
	int token;
	int param_counter;
	int dimsize[20];
	int dim;
	int dimcall[20];
	Ilist* INTlist;
	Ilist* INTlist_tail;
	element_value literal;
} dict_element;

typedef struct _ast_element
{
	int AST_TYPE;
	bool semelse;
	char code[10000];
	char reg[10];
	dict_element* entry;
} ast_element;

typedef struct _comp_stack_t {
	comp_dict_t *dict;
	struct _comp_stack_t *next;
} comp_stack_t;

ast_element* createAstElement(int AST_TYPE , dict_element* element);

comp_tree_t* astSymbolEntry(dict_element* element, int AST_TYPE);

void tree_free_element(comp_tree_t *tree);

comp_stack_t* stack_new();
void stack_free(comp_stack_t* stack);
//void push(comp_stack_t* stack, comp_dict_t *dict);
void push(comp_stack_t** stack);
void pop(comp_stack_t** stack);

void declaration_set(dict_element** element, comp_stack_t* stack);
void declaration_test(dict_element** element, comp_stack_t* stack);

void variable_set(dict_element** element);
void variable_test(dict_element* element); //Erro caso NAO seja uma variavel

void vector_set(dict_element** element);
void vector_test(dict_element* element); //Erro caso NAO seja um vetor

void function_set(dict_element** element);
void function_test(dict_element* element); //Erro caso NAO seja uma funcao

void type_set(dict_element** element, int type); //type: IKS_INT IKS_FLOAT IKS_CHAR IKS_STRING IKS_BOOL
void coertion_check(dict_element** father_element, dict_element* left, dict_element* right); //type: IKS_INT IKS_FLOAT IKS_BOOL

void inherit_coertion(dict_element** father, dict_element* son); //passa parâmetros do filho para o pai
void vector_index_check(dict_element* element);
int vector_dim_size(dict_element* element);
void vector_dim_set(dict_element** element, int dim, int* vec, int vec_size);
void return_test(dict_element* element, int function_type); // Funcao feita apra verificar se tipo do retorno eh o mesmo da funcao. Verifica se parametro do elemento é igual ao passado no segundo parametro

void function_param_set(dict_element** element, int type);
void function_param_test();
void function_param_debug(dict_element* element);

void function_param_correct_order(dict_element** element);

Ilist* param_list_get(dict_element* element);
Ilist* param_check(dict_element* element, Ilist* cursor);
void param_count_check(dict_element* declared, int called);
void element_debug_print(dict_element* element);

void input_test(dict_element* element);
void output_test(dict_element* element);

void boolean_test(dict_element* element);

void codeGen(comp_tree_t* tree_a);

void codeArim(comp_tree_t* tree_a);
void codeAtrib(comp_tree_t* tree_a);
void codeProg(comp_tree_t* tree_a);
void codeFunc(comp_tree_t* tree_a);
void codeIf(comp_tree_t* tree_a);
void codeDoWhile(comp_tree_t* tree_a);
void codeWhileDo(comp_tree_t* tree_a);
void codeBlock(comp_tree_t* tree_a);
void codeLit(comp_tree_t* tree_a);
void codeIdent(comp_tree_t* tree_a);
void codeInv(comp_tree_t* tree_a);
void codeAnd(comp_tree_t* tree_a);
void codeOr(comp_tree_t* tree_a);
void codeComp(comp_tree_t* tree_a);
void codeNot(comp_tree_t* tree_a);
void codeVec(comp_tree_t* tree_a);

void newLabel (char* dest);
void newRegister (char* dest);
unsigned long long matrixAddressCalc(dict_element* element, int* vec);

#endif

